package br.com.sysbirth.view;

import br.com.sysbirth.entity.Customer;
import br.com.sysbirth.entity.Project;
import br.com.sysbirth.repository.CustomerRepository;
import br.com.sysbirth.repository.ProjectRepository;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class CustomerView implements Serializable {
    
    @Inject
    private CustomerRepository customerRepository;
    @Inject
    private ProjectRepository projectRepository;
    
    public void save() {
        Customer customer = new Customer();
        customerRepository.add(customer);
    }
    
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }
    
}
