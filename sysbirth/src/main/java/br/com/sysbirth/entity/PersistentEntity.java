package br.com.sysbirth.entity;

/**
 *
 * Interface to define entity classes
 * 
 * @author jose.bonifacio
 */
public interface PersistentEntity {
    
    public Long getId();
    public void setId(Long id);
    public boolean isNew();    
    
}
