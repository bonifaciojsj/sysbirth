package br.com.sysbirth.entity;

import javax.persistence.*;

/**
 *
 * This class represent the Customers of this software
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "customer")
public class Customer implements PersistentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
        
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }    
        
    @Override
    public boolean isNew() {
        return this.id == null;
    }

}
