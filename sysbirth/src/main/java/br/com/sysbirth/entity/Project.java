package br.com.sysbirth.entity;

import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "project")
public class Project implements PersistentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;
        
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }    

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
        
    @Override
    public boolean isNew() {
        return this.id == null;
    }
}
