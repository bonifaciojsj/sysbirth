package br.com.sysbirth.repository;

import br.com.sysbirth.entity.Customer;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class CustomerRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Customer add(Customer customer) {
        if (customer.getId() == null) {
            entityManager.persist(customer);
            entityManager.flush();
        } else {
            entityManager.merge(customer);
        }
        return customer;
    }

    public void remove(Customer customer) {
        customer = entityManager.getReference(Customer.class, customer);
        entityManager.remove(customer);
    }

    public Customer get(Long id) {
        return entityManager.find(Customer.class, id);
    }

    public List<Customer> findAll() {
        return entityManager.createQuery("SELECT c FROM Customer c").getResultList();
    }
}
