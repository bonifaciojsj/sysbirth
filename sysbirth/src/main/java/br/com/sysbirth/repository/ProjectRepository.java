package br.com.sysbirth.repository;

import br.com.sysbirth.entity.Project;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class ProjectRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Project add(Project project) {
        if (project.getId() == null) {
            entityManager.persist(project);
            entityManager.flush();
        } else {
            entityManager.merge(project);
        }
        return project;
    }

    public void remove(Project project) {
        project = entityManager.getReference(Project.class, project);
        entityManager.remove(project);
    }

    public Project get(Long id) {
        return entityManager.find(Project.class, id);
    }

    public List<Project> findAll() {
        return entityManager.createQuery("SELECT p FROM Project p LEFT JOIN FETCH p.customer").getResultList();
    }

}
