DROP TABLE project;
drop table customer;

CREATE TABLE customer (
id bigserial not null primary key
);

CREATE TABLE project (
id bigserial not null primary key,
customer_id bigserial references customer(id)
);
